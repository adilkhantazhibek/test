<?php

namespace App\Http\Controllers\Api\V1;

use App\Department;
use App\Http\Controllers\Controller;
use App\Staff;
use Illuminate\Http\Request;

class StaffController extends SiteController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff = $this->getStaff();

        return response()->json(['staff' => $staff], '200');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (count(Department::all()) > 0){

            return response()->json(['status' => 'create'], 200);
        }

        return response()->json(['status' => null], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $staff = new Staff();

        if($request->name)
            $staff->name = $request->name;

        if($request->surname)
            $staff->surname = $request->surname;

        if($request->middle_name)
            $staff->middle_name = $request->middle_name;

        if($request->male)
            $staff->male = $request->male;

        if($request->salary)
            $staff->salary = $request->salary;

        $staff->save();

        $staff->departments()->attach(array_unique($request->departments_id));


        return response()->json('ok',200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staff = Staff::findOrFail($id);

        return response()->json($staff);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff = Staff::findOrFail($id);
        $staff->departments_id = Staff::departments_id($id);

        return response()->json(['staff' => $staff]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $staff = Staff::findOrFail($id);

        if($request->name)
            $staff->name = $request->name;

        if($request->surname)
            $staff->surname = $request->surname;

        if($request->middle_name)
            $staff->middle_name = $request->middle_name;

        if($request->male)
            $staff->male = $request->male;

        if($request->salary)
            $staff->salary = $request->salary;

        $staff->save();

        if (count($staff->departments) > 0)
            $staff->departments()->detach();

        $staff->departments()->attach(array_unique($request->departments_id));

        return response()->json('', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff = Staff::findOrFail($id);

        if ($staff->delete()){
            return response()->json('', 200);
        }

        return response()->json(['status' => 'success']);
    }
}
